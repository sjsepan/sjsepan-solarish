# Solarish Color Theme - Change Log

## [0.1.3]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.2]

- update readme and screenshot

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- buttons border
- fix manifest repo links
- consistent badges
- retain v0.0.3 for those that prefer earlier style

## [0.0.3]

- fix scrollbar, minimap slider transparencies:
light:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",

## [0.0.2]

- fix pkg screenshot url
- fix list.activeSelection FG/BG

## [0.0.1]

- editor widget FG/BG
- Initial release
